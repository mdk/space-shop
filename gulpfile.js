var gulp = require('gulp');
var gzip = require('gulp-gzip');
var replace = require('gulp-string-replace');

gulp.task('compress', function() {
  gulp.src(['./dist/*.js', './dist/*.css'])
    .pipe(gzip())
    .pipe(gulp.dest('./dist'));
});

gulp.task('replace', function() {
  gulp.src(["dist/index.html"], {base: "./"})
    .pipe(replace(/bundle.js/g, 'bundle.js.gz'))
    .pipe(replace(/bundle.css/g, 'bundle.css.gz'))
    .pipe(gulp.dest('./'))
});
