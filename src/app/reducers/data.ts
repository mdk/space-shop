import {Action} from '@ngrx/store';
import * as dataModel from '../model/data';
import * as data from '../actions/data';
import {merge, clone, without, trim, find} from 'lodash';

export function reducer(state = dataModel.defaults, action: Action): dataModel.Data {
  const stateCopy = clone(state);

  switch (action.type) {
    case data.ActionTypes.LOAD_USERS_SUCCESS:
      stateCopy.users = action.payload;
      return merge({}, stateCopy);
    case data.ActionTypes.SET_CURRENT_USER:
      return merge({}, state, {currentUser: action.payload});
    case data.ActionTypes.LOAD_PRODUCTS_SUCCESS:
      stateCopy.products = action.payload;
      return merge({}, stateCopy);
    case data.ActionTypes.LOAD_ORDERS_SUCCESS:
      stateCopy.orders = action.payload;
      return merge({}, stateCopy);
    default:
      return state;
  }
}

export const getUsers = (state: dataModel.Data) => state.users;
export const getCurrentUser = (state: dataModel.Data) => state.currentUser;
export const getProducts = (state: dataModel.Data) => state.products;
export const getOrders = (state: dataModel.Data) => state.orders;

