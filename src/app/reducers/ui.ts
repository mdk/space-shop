import {Action} from '@ngrx/store';
import * as uiModel from '../model/ui';
import * as ui from '../actions/ui';
import {merge, clone, without, trim} from 'lodash';

export function reducer(state = uiModel.defaults, action: Action): uiModel.UI {
  const stateCopy = clone(state);

  switch (action.type) {
    case ui.ActionTypes.SET_TOOLBAR_COLOR:
      return merge({}, state, {toolbarColor: action.payload});
    case ui.ActionTypes.SET_PHOTO_URL:
      let photoURL = action.payload;
      if (!photoURL) {
        photoURL = 'assets/images/user.png';
      }
      return merge({}, state, {photoURL: photoURL});
    case ui.ActionTypes.SET_CART:
      let cart = action.payload;
      if (!cart) {
        cart = {fullPrice: 0, productList: {}, count: 0};
      }
      stateCopy.cart = cart;
      return stateCopy;
    case ui.ActionTypes.ADD_PRODUCT_TO_CART:
      const product = action.payload;
      let currentCart = clone(state.cart);
      if (currentCart) {
        currentCart.productList = clone(state.cart.productList);
      } else {
        currentCart = {fullPrice: 0, count: 0, productList: {}};
      }
      if (product) {
        let count = 1;
        const existedProduct = currentCart.productList[product.uid];
        if (existedProduct && !isNaN(existedProduct)) {
          count = 1 + existedProduct;
        }
        currentCart.productList[product.uid] = count;
        currentCart.fullPrice = parseInt(currentCart.fullPrice.toString(), 0)
          + ((product.price && !isNaN(product.price)) ? parseInt(product.price, 0) : 0);
        currentCart.count = currentCart.count + 1;
      }
      return merge({}, state, {cart: currentCart});
    default:
      return state;
  }
}

export const getToolbarColor = (state: uiModel.UI) => state.toolbarColor;
export const getPhotoURL = (state: uiModel.UI) => state.photoURL;
export const getCart = (state: uiModel.UI) => state.cart;
