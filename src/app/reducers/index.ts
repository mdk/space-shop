import {createSelector} from 'reselect';
import {ActionReducer, combineReducers} from '@ngrx/store';
import {compose} from '@ngrx/core/compose';
import {storeLogger} from 'ngrx-store-logger';
import {storeFreeze} from 'ngrx-store-freeze';

import {environment} from '../../environments/environment';

import * as dataModel from '../model/data';
import * as uiModel from '../model/ui';
import * as fromData from './data';
import * as fromUi from './ui';
import {localStorageSync} from 'ngrx-store-localstorage';

export interface State {
  data: dataModel.Data;
  ui: uiModel.UI;
}

const reducers = {
  data: fromData.reducer
  , ui: fromUi.reducer
};

// const developmentReducer: ActionReducer<State> = compose(storeLogger(), combineReducers)(reducers);
const developmentReducer: ActionReducer<State> = compose(
  storeLogger()
  , localStorageSync({keys: ['ui'], rehydrate: true})
  , storeFreeze
  , combineReducers
)(reducers);
const productionReducer: ActionReducer<State> = compose(
  // storeLogger()
  localStorageSync({keys: ['ui'], rehydrate: true})
  // , storeFreeze
  , combineReducers
)(reducers);

export function reducer(state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}

/* Data */
export const getDataState = (state: State) => state.data;
export const getUsers = createSelector(getDataState, fromData.getUsers);
export const getCurrentUser = createSelector(getDataState, fromData.getCurrentUser);
export const getProducts = createSelector(getDataState, fromData.getProducts);
export const getOrders = createSelector(getDataState, fromData.getOrders);

/* UI */
export const getUIState = (state: State) => state.ui;
export const getToolbarColor = createSelector(getUIState, fromUi.getToolbarColor);
export const getPhotoURL = createSelector(getUIState, fromUi.getPhotoURL);
export const getCart = createSelector(getUIState, fromUi.getCart);
