import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {ToasterService} from 'angular2-toaster';
import {OrdersDataService} from './orders-data.service';
import {of} from 'rxjs/observable/of';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import * as data from '../actions/data';
import * as ui from '../actions/ui';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/switchMap';
import {mapKeys, keys, isObject} from 'lodash';
import {Router} from '@angular/router';
import {CommonService} from '../core/common.service';

@Injectable()
export class OrdersDataEffects {

  constructor(private actions$: Actions
    , private common: CommonService
    , private dataService: OrdersDataService
    , private toasterService: ToasterService
    , private router: Router) {
  }

  @Effect()
  load$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.LOAD_ORDERS)
    .debounceTime(300)
    .map((action: data.UpdateOrdersAction) => action.payload)
    .switchMap(payload => this.dataService.load()
      .map(res => new data.LoadOrdersSuccessAction(res))
      .catch(err => of(new data.OrdersServerFailAction(err)))
    );

  @Effect()
  create$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.CREATE_ORDER)
    .debounceTime(300)
    .map((action: data.UpdateOrdersAction) => action.payload)
    .switchMap(payload => this.dataService.create(payload)
      .then(res => {
        return this.router.navigate(['/orders']).then(() =>
          this.common.showMessage('info', 'Order successfully created. We will contact you soon!')
        ).then(() =>
          new ui.SetCartAction(null)
        );
      })
      .catch(err => of(new data.OrdersServerFailAction(err)))
    );

  @Effect()
  remove$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.REMOVE_ORDER)
    .debounceTime(300)
    .map((action: data.UpdateOrdersAction) => action.payload)
    .switchMap(payload => this.dataService.remove(payload)
      .then(res => new data.ChangeOrderSuccessAction(payload))
      .catch(err => of(new data.OrdersServerFailAction(err)))
    );

  @Effect()
  change$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.CHANGE_ORDER)
    .debounceTime(300)
    .map((action: data.UpdateOrdersAction) => action.payload)
    .switchMap(payload => this.dataService.update(payload)
      .then(res => new data.ChangeOrderSuccessAction(payload))
      .catch(err => of(new data.OrdersServerFailAction(err)))
    );

  @Effect({dispatch: false})
  serverFail$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.ORDERS_SERVER_FAIL)
    .debounceTime(300)
    .map((action: data.UpdateOrdersAction) => action.payload)
    .switchMap(payload => {
      this.toasterService.pop('error', 'Failure',
        isObject(payload.error) ? keys(
          mapKeys(payload.error, (value: Array<string>, key: string) => `${key}: ${value.join(';')}`)).join(';') :
          payload.error);
      return of({});
    });
}
