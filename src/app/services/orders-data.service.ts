import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import * as data from '../actions/data';
import {CommonService} from '../core/common.service';
import {Order} from '../model/Order';

@Injectable()
export class OrdersDataService {
  // private orders: FirebaseListObservable<any>;

  constructor(private afDB: AngularFireDatabase
    , private store: Store<fromRoot.State>
    , private common: CommonService) {
    // this.orders = this.afDB.list('orders');
  }

  dispatchLoad() {
    this.store.dispatch(new data.LoadOrdersAction());
  }

  load() {
    return this.afDB.list('orders');
    // return this.orders;
  }

  create(payload) {
    const order = new Order(payload);
    const ref = this.afDB.database.ref('orders').push();
    const id = ref.key;
    order.uid = id;
    return this.afDB.object('orders/' + id).set(order);
  }

  remove(payload) {
    return this.afDB.object('orders/' + payload).set(null);
  }

  update(order) {
    const uid = order.uid;
    return this.afDB.object(`orders/${uid}`).update(order);
  }

}
