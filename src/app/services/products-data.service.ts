import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import * as data from '../actions/data';
import {CommonService} from '../core/common.service';
import {Product} from '../model/Product';

@Injectable()
export class ProductsDataService {
  private products: FirebaseListObservable<any>;

  constructor(private afDB: AngularFireDatabase
    , private store: Store<fromRoot.State>
    , private common: CommonService) {
    this.products = this.afDB.list('products');
  }

  dispatchLoad() {
    this.store.dispatch(new data.LoadProductsAction());
  }

  load() {
    return this.products;
  }

  create(payload) {
    const product = new Product({
      title: payload.title,
    });
    if (payload.comment) {
      product.comment = payload.comment;
    }
    if (payload.description) {
      product.description = payload.description;
    }
    if (payload.price) {
      product.price = payload.price;
    }
    if (payload.photoURL) {
      product.photoURL = payload.photoURL;
    }
    const promise = this.products.push(product);
    const id = promise.key;
    product.uid = id;
    this.afDB.object('products/' + id + '/uid').set(id);
    return promise;
  }

  remove(payload) {
    return this.afDB.object('products/' + payload).set(null);
  }

  update(product) {
    const uid = product.uid;
    return this.afDB.object(`products/${uid}`).update(product);
  }

}
