import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import * as data from '../actions/data';
import {AuthenticationService} from '../login/authentication.service';
import {CommonService} from '../core/common.service';

@Injectable()
export class UsersDataService {
  private users: FirebaseListObservable<any>;

  constructor(private afDB: AngularFireDatabase
    , private authenticationService: AuthenticationService
    , private store: Store<fromRoot.State>
    , private common: CommonService) {
    this.users = this.afDB.list('users');
  }

  dispatchLoad() {
    this.store.dispatch(new data.LoadUsersAction());
  }

  load() {
    return this.users;
  }

  create(payload) {
    return this.authenticationService.registerUser(payload.email, payload.password, false).then((user) => {
      this.authenticationService.saveUserInfoFromForm(
        user.uid
        , payload.displayName
        , payload.email
        , payload.isAdmin
        , payload.isModerator
        , payload.billingAddress
        , payload.shippingAddress
      ).then(() => {
      })
        .catch((error) => {
          this.common.showError(error);
        });
    })
      .catch((error) => {
        this.common.showError(error);
      });
  }

  remove(payload) {
    return this.authenticationService.removeUserInfoFromDB(payload).then(() => {
      this.common.showMessage('info',
        `User removed from DB. But you still need to remove it from Firebase Authentication section manually here: https://console.firebase.google.com/project/space-shop/authentication/users`);
    });
  }

  update(user) {
    const uid = user.uid;
    return this.afDB.object(`users/${uid}`).update(user);
  }

}
