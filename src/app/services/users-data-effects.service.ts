import { Injectable } from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {ToasterService} from 'angular2-toaster';
import {UsersDataService} from './users-data.service';
import {of} from 'rxjs/observable/of';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import * as data from '../actions/data';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/switchMap';
import {mapKeys, keys, isObject} from 'lodash';

@Injectable()
export class UsersDataEffects {

  constructor(private actions$: Actions, private dataService: UsersDataService, private toasterService: ToasterService) { }

  @Effect()
  load$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.LOAD_USERS)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => this.dataService.load()
      .map(res => new data.LoadUsersSuccessAction(res))
      .catch(err => of(new data.UsersServerFailAction(err)))
    );

  @Effect()
  create$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.CREATE_USER)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => this.dataService.create(payload)
      .then(res => new data.CreateUserSuccessAction(res))
      .catch(err => of(new data.UsersServerFailAction(err)))
    );

  @Effect()
  remove$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.REMOVE_USER)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => this.dataService.remove(payload)
      .then(res => new data.RemoveUserSuccessAction(payload))
      .catch(err => of(new data.UsersServerFailAction(err)))
    );

  @Effect()
  change$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.CHANGE_USER)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => this.dataService.update(payload)
      .then(res => new data.ChangeUserSuccessAction(payload))
      .catch(err => of(new data.UsersServerFailAction(err)))
    );

  @Effect({dispatch: false})
  serverFail$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.USERS_SERVER_FAIL)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => {
      this.toasterService.pop('error', 'Failure',
        isObject(payload.error) ? keys(
          mapKeys(payload.error, (value: Array<string>, key: string) => `${key}: ${value.join(';')}`)).join(';') :
          payload.error);
      return of({});
    });
}
