import { Injectable } from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {ToasterService} from 'angular2-toaster';
import {of} from 'rxjs/observable/of';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import * as data from '../actions/data';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/switchMap';
import {mapKeys, keys, isObject} from 'lodash';
import {ProductsDataService} from './products-data.service';

@Injectable()
export class ProductsDataEffects {

  constructor(private actions$: Actions, private dataService: ProductsDataService, private toasterService: ToasterService) { }

  @Effect()
  load$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.LOAD_PRODUCTS)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => this.dataService.load()
      .map(res => new data.LoadProductsSuccessAction(res))
      .catch(err => of(new data.ProductServerFailAction(err)))
    );

  @Effect()
  create$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.CREATE_PRODUCT)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => this.dataService.create(payload)
      .then(res => new data.ChangeProductSuccessAction(payload))
      .catch(err => of(new data.ProductServerFailAction(err)))
    );

  @Effect()
  remove$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.REMOVE_PRODUCT)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => this.dataService.remove(payload)
      .then(res => new data.ChangeProductSuccessAction(payload))
      .catch(err => of(new data.ProductServerFailAction(err)))
    );

  @Effect()
  change$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.CHANGE_PRODUCT)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => this.dataService.update(payload)
      .then(res => new data.ChangeProductSuccessAction(payload))
      .catch(err => of(new data.ProductServerFailAction(err)))
    );

  @Effect({dispatch: false})
  serverFail$: Observable<Action> = this.actions$
    .ofType(data.ActionTypes.PRODUCTS_SERVER_FAIL)
    .debounceTime(300)
    .map((action: data.UpdateAction) => action.payload)
    .switchMap(payload => {
      this.toasterService.pop('error', 'Failure',
        isObject(payload.error) ? keys(
          mapKeys(payload.error, (value: Array<string>, key: string) => `${key}: ${value.join(';')}`)).join(';') :
          payload.error);
      return of({});
    });
}
