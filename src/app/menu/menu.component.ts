import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import {AppUser} from '../model/AppUser';
import 'rxjs/add/operator/takeWhile';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {
  public currentUser: AppUser;
  public alive = true;

  constructor(
    private store: Store<fromRoot.State>
  ) {
    this.store.select(fromRoot.getCurrentUser)
      .takeWhile(() => this.alive)
      .subscribe(user => {
        this.currentUser = user;
      });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
