export class AppUser {
  uid: string = null;
  email: string = null;
  displayName: string = null;
  billingAddress?: string = null;
  shippingAddress?: string = null;
  isAdmin?: boolean = null;
  isModerator?: boolean = null;
  photoURL?: string = null;


  constructor(obj?: any) {
    if (!obj) {
      return;
    }
    if (obj.uid) {
      this.uid = obj.uid;
    }
    if (obj.email) {
      this.email = obj.email;
    }
    if (obj.displayName) {
      this.displayName = obj.displayName;
    }
    if (obj.billingAddress) {
      this.billingAddress = obj.billingAddress;
    }
    if (obj.shippingAddress) {
      this.shippingAddress = obj.shippingAddress;
    }
    if (obj.isAdmin) {
      this.isAdmin = obj.isAdmin;
    }
    if (obj.isModerator) {
      this.isModerator = obj.isModerator;
    }
    if (obj.photoURL) {
      this.photoURL = obj.photoURL;
    }
  }
}
