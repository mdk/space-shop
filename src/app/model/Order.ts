export class Order {
  uid: string = null;
  userId: string = null;
  userName: string = null;
  billingAddress?: string = null;
  shippingAddress?: string = null;
  productList: {};
  comment?: string = null;
  fullPrice: number = null;
  status: string = null;
  token?: string = null;

  constructor(obj?: any) {
    if (!obj) {
      return;
    }
    if (obj.uid) {
      this.uid = obj.uid;
    }
    if (obj.userId) {
      this.userId = obj.userId;
    }
    if (obj.userName) {
      this.userName = obj.userName;
    }
    if (obj.comment) {
      this.comment = obj.comment;
    }
    if (obj.billingAddress) {
      this.billingAddress = obj.billingAddress;
    }
    if (obj.shippingAddress) {
      this.shippingAddress = obj.shippingAddress;
    }
    if (obj.fullPrice) {
      this.fullPrice = obj.fullPrice;
    }
    if (obj.status) {
      this.status = obj.status;
    }
    if (obj.productList) {
      this.productList = obj.productList;
    }
    if (obj.token) {
      this.token = obj.token;
    }
  }
}
