import {Cart} from './Cart';

export interface UI {
  toolbarColor: string;
  photoURL: string;
  cart: Cart;
}

export const defaults: UI = {
  toolbarColor: '#292b2c'
  , photoURL: null
  , cart: {fullPrice: 0, productList: {}, count: 0}
};
