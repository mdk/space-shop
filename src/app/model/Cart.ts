export class Cart {
  productList: any = {};
  fullPrice = 0;
  count = 0;

  constructor(obj?: any) {
    if (!obj) {
      return;
    }
    if (obj.fullPrice) {
      this.fullPrice = obj.fullPrice;
    }
    if (obj.count) {
      this.count = obj.count;
    }
    if (obj.productList) {
      this.productList = obj.productList;
    }
  }
}
