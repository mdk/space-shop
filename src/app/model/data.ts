import {AppUser} from './AppUser';
import {Product} from './Product';
import {Order} from './Order';

export interface Data {
  currentUser: AppUser;
  users: Array<AppUser>;
  products: Array<Product>;
  orders: Array<Order>;
}

export const defaults: Data = {
  currentUser: null
  , users: []
  , products: []
  , orders: []
};
