export class Product {
  uid: string = null;
  title: string = null;
  comment?: string = null;
  description?: string = null;
  price: number = null;
  photoURL?: string;

  constructor(obj?: any) {
    if (!obj) {
      return;
    }
    if (obj.uid) {
      this.uid = obj.uid;
    }
    if (obj.title) {
      this.title = obj.title;
    }
    if (obj.comment) {
      this.comment = obj.comment;
    }
    if (obj.description) {
      this.description = obj.description;
    }
    if (obj.price) {
      this.price = obj.price;
    }
    if (obj.photoURL) {
      this.photoURL = obj.photoURL;
    }
  }
}
