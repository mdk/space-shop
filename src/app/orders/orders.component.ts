import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as fromRoot from '../reducers';
import {Store} from '@ngrx/store';
import 'rxjs/add/operator/takeWhile';
import {Order} from '../model/Order';
import {OrdersDataService} from '../services/orders-data.service';
import {MdDialog, MdPaginator} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {OrderDialogComponent} from '../components/order-dialog/order-dialog.component';
import {CommonService} from '../core/common.service';
import * as data from '../actions/data';
import {AppUser} from '../model/AppUser';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, OnDestroy {
  public alive = true;
  displayedColumns = ['userName', 'shippingAddress', 'fullPrice', 'status', 'actions'];
  dataSource: OrdersDataSource | null;

  @ViewChild(MdPaginator) paginator: MdPaginator;
  private currentUser: AppUser;

  constructor(private store: Store<fromRoot.State>
    , private dataService: OrdersDataService
    , public dialog: MdDialog
    , public common: CommonService) {
    this.store.select(fromRoot.getCurrentUser)
      .takeWhile(() => this.alive)
      .subscribe(user => {
        this.currentUser = user;
      });
  }

  ngOnInit() {
    this.dataService.dispatchLoad();
    this.dataSource = new OrdersDataSource(this.store, this.paginator, this.common);
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  editOrder(row: any) {
    console.log('Edit: ' + JSON.stringify(row));
    const dialogRef = this.dialog.open(OrderDialogComponent, {
      width: '350px',
      disableClose: true,
      data: {
        order: row
        , edit: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Edit dialog was closed: ' + JSON.stringify(result));
      if (result) {
        this.store.dispatch(new data.ChangeOrderAction(result));
      }
    });
  }

  removeOrder(row: any) {
    this.common.confirm('Remove Order', `Are you sure you want to delete order?`)
      .take(1)
      .subscribe(res => {
        console.log('Remove: ' + JSON.stringify(res));
        if (res && row.uid) {
          this.store.dispatch(new data.RemoveOrderAction(row.uid));
        }
      });
  }

/*
  createOrder() {
    const dialogRef = this.dialog.open(OrderDialogComponent, {
      width: '350px',
      disableClose: true,
      data: {order: {}}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Create dialog was closed: ' + JSON.stringify(result));
      if (result) {
        this.store.dispatch(new data.CreateOrderAction(result));
      }
    });
  }
*/
}

export class OrdersDataSource extends DataSource<any> implements OnDestroy {
  public alive = true;
  public orders: Array<Order> = [];
  dataChange: BehaviorSubject<Order[]> = new BehaviorSubject<Order[]>([]);

  constructor(private store: Store<fromRoot.State>
              , private _paginator: MdPaginator
              , private common: CommonService
  ) {
    super();
    this.store.select(fromRoot.getOrders)
      .takeWhile(() => this.alive)
      .subscribe(val => {
        this.orders = val.filter(order => {
          const currentUser = this.common.auth.appUser;
          return currentUser && (currentUser.isAdmin || currentUser.isModerator || (order.userId === currentUser.uid));
        });
        this.dataChange.next(this.orders);
      });
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Order[]> {
    const displayDataChanges = [
      this.dataChange,
      this._paginator.page,
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      const data = this.orders.slice();

      // Grab the page's slice of data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      return data.splice(startIndex, this._paginator.pageSize);
    });
  }

  disconnect() {
  }
}
