import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './orders.component';
import {RouterModule, Routes} from '@angular/router';
import {CanActivateAuthGuard} from '../login/can-activate.authguard';
import {EffectsModule} from '@ngrx/effects';
import {
  MdButtonModule, MdDialogModule, MdFormFieldModule, MdIconModule, MdInputModule, MdPaginatorModule,
  MdTableModule, MdCheckboxModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {ComponentsModule} from '../components/components.module';
import {OrdersDataEffects} from '../services/orders-data-effects.service';

const routes: Routes = [
  {path: '', component: OrdersComponent, canActivate: [CanActivateAuthGuard]}
];

@NgModule({
  imports: [
    CommonModule
    , FormsModule
    , RouterModule.forChild(routes)
    , ComponentsModule
    , MdTableModule
    , MdPaginatorModule
    , MdButtonModule
    , MdFormFieldModule
    , MdInputModule
    , MdIconModule
    , MdDialogModule
    , MdCheckboxModule
  ],
  declarations: [
    OrdersComponent
  ]
})
export class OrdersModule { }
