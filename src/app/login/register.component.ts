import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {CommonService} from '../core/common.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  private captchaResponse = null;

  constructor(private authenticationService: AuthenticationService
              , private router: Router
              , private common: CommonService
  ) {
  }

  // registers the user and logs them in
  register(event, name, email, password) {
    event.preventDefault();
    if (!this.captchaResponse) {
      this.common.showError('reCAPTCHA must be filled correctly!');
    } else {
      this.authenticationService.registerUser(email, password).then((user) => {
        this.authenticationService.saveUserInfoFromForm(user.uid, name, email).then(() => {
          this.router.navigate(['']);
        })
          .catch((error) => {
            this.common.showError(error);
          });
      })
        .catch((error) => {
          this.common.showError(error);
        });
    }
  }

  ngOnInit() {
    this.captchaResponse = null;
  }

  captchaResolved(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
    console.log(`Resolved captcha with response ${captchaResponse}:`);
  }

}
