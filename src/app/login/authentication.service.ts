import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {UtilsService} from '../core/utils.service';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {Observable} from 'rxjs/Observable';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import {AppUser} from '../model/AppUser';
import {Store} from '@ngrx/store';
import * as ui from '../actions/ui';
import * as data from '../actions/data';
import * as fromRoot from '../reducers';
import {Router} from '@angular/router';

@Injectable()
export class AuthenticationService {
  private secondaryApp = null;
  public users: FirebaseListObservable<any>;
  public displayName: string;
  public email: string;
  public appUser: AppUser;
  private authenticated: Promise<AppUser>;

  constructor(private utils: UtilsService, private afAuth: AngularFireAuth, private afDB: AngularFireDatabase
    , private store: Store<fromRoot.State>
    , private router: Router
  ) {
    this.users = this.afDB.list('users');
    this.authenticated = new Promise((resolve, reject) => {
      this.afAuth.authState.subscribe(
        (auth) => {
          if (auth == null) {
            console.log('Not Logged in.');
          } else {
            console.log('Successfully Logged in.');
            const service = this;
            service.displayName = auth.displayName;
            service.email = auth.email;
            auth.providerData.forEach(function (profile) {
              service.displayName = profile.displayName;
              service.email = profile.email;
            });
            if (!service.displayName) {
              service.displayName = service.email;
            }
            this.afDB.object('users/' + auth.uid).take(1).subscribe(user => {
              this.appUser = new AppUser(user);
              let needChange = false;
              if (!this.appUser.uid) {
                this.appUser.uid = auth.uid;
                needChange = true;
              }
              // noinspection TsLint
              if (service.displayName && !this.appUser.displayName) {
                this.appUser.displayName = service.displayName;
                needChange = true;
              }
              // noinspection TsLint
              if (this.appUser.email != service.email) {
                this.appUser.email = service.email;
                needChange = true;
              }
              // noinspection TsLint
              if (this.appUser.photoURL != auth.photoURL) {
                this.appUser.photoURL = auth.photoURL;
                needChange = true;
              }
              if (needChange) {
                this.afDB.object('users/' + this.appUser.uid).set(this.appUser);
              }
              this.store.dispatch(new ui.SetPhotoURLAction(this.appUser.photoURL));
              resolve(this.appUser);
            });
            this.afDB.object('users/' + auth.uid).subscribe(user => {
              this.store.dispatch(new data.SetCurrentUserAction(user));
            });
          }
        }
      );
    });
  }

  getAuthenticatedUser(): Promise<AppUser> {
    return this.authenticated;
  }

  getAuthenticated(): Observable<any> {
    return this.afAuth.authState;
  }

  loginWithGoogle() {
    return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    return this.afAuth.auth.signOut().then(() => this.router.navigate(['/login']));
  }

  registerUser(email, password, autoSignIn = true) {
    console.log(email);
    if (autoSignIn) {
      return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
    } else {
      const secondaryApp = this.getCreateUsersFirebaseApp();
      return secondaryApp.auth().createUserWithEmailAndPassword(email, password).then(function (user) {
        secondaryApp.auth().signOut();
        return user;
      });
    }
  }

  private getCreateUsersFirebaseApp() {
    if (!this.secondaryApp) {
      this.secondaryApp = firebase.initializeApp(this.utils.getEnv().firebase, 'Create Users App');
    }
    return this.secondaryApp;
  }

  saveUserInfoFromForm(uid, displayName, email
    , isAdmin?: boolean, isModerator?: boolean
    , billingAddress?: string, shippingAddress?: string
    , photoURL?: string) {
    const user = new AppUser({
      uid: uid
      , email: email
      , displayName: displayName
    });
    if (isAdmin) {
      user.isAdmin = isAdmin;
    }
    if (isModerator) {
      user.isModerator = isModerator;
    }
    if (billingAddress) {
      user.billingAddress = billingAddress;
    }
    if (shippingAddress) {
      user.shippingAddress = shippingAddress;
    }
    if (photoURL) {
      user.photoURL = photoURL;
    }
    return this.afDB.object('users/' + uid).set(user);
  }

  loginWithEmail(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  removeUserInfoFromDB(uid: string) {
    return this.afDB.object('users/' + uid).set(null);
  }
}
