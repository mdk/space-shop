import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {CommonService} from '../core/common.service';


@Component({
  moduleId: module.id,
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit {
  loading = false;

  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private common: CommonService) {
  }

  ngOnInit(): void {
  }

  loginWithGoogle() {
    this.authenticationService.loginWithGoogle().then((data) => {
      this.router.navigate(['']);
    });
  }

  loginWithAdmin() {
    this.loginWithEmail(null, 'admin@example.com', '123456');
  }

  loginWithEmail(event, email, password) {
    if (event) {
      event.preventDefault();
    }
    this.authenticationService.loginWithEmail(email, password).then(() => {
      this.router.navigate(['']);
    })
      .catch((error: any) => {
        if (error) {
          this.common.showError(error);
        }
      });
  }
}
