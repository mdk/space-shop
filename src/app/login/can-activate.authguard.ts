import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/take';

@Injectable()
export class CanActivateAuthGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthenticationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return this.authService.getAuthenticatedUser().then(user => {
      if (user == null) {
        this.router.navigate(['/login']);
        return false;
      }
      if (state.url && state.url.toLowerCase().startsWith('/users') && !user.isAdmin) {
        this.router.navigate(['']);
        return false;
      }
      return true;
    });
  }
}
