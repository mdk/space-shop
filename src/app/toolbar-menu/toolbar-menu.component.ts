import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import {Observable} from 'rxjs/Observable';
import {AuthenticationService} from '../login/authentication.service';
import {MdDialog} from '@angular/material';
import {EditDialogComponent} from '../components/user-edit-dialog/edit-dialog.component';
import {AppUser} from '../model/AppUser';
import * as data from '../actions/data';
import {Cart} from '../model/Cart';
import {CartDialogComponent} from '../components/cart-dialog/cart-dialog.component';
import {Order} from '../model/Order';
import {CommonService} from '../core/common.service';

@Component({
  selector: 'app-toolbar-menu',
  templateUrl: './toolbar-menu.component.html',
  styleUrls: ['./toolbar-menu.component.scss']
})
export class ToolbarMenuComponent implements OnInit, OnDestroy {
  title = 'Space Shop';
  public alive = true;
  public photoURL$: Observable<string>;
  public currentUser: AppUser;
  public auth: any;
  public cart: Cart;

  constructor(private store: Store<fromRoot.State>
    , private authenticationService: AuthenticationService
    , private common: CommonService
    , public dialog: MdDialog) {
    this.photoURL$ = this.store.select(fromRoot.getPhotoURL);
    this.store.select(fromRoot.getCurrentUser)
      .subscribe(user =>
        this.currentUser = user
      );
    this.store.select(fromRoot.getCart)
      .subscribe(cart =>
        this.cart = cart
      );
    this.authenticationService.getAuthenticated().subscribe((auth) => {
      this.auth = auth;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  onCartClick() {
    console.log('Cart');
    if (!this.cart || (this.cart && !this.cart.count)) {
      this.common.showMessage('warning', 'Cart is empty');
      return;
    }
    const dialogRef = this.dialog.open(CartDialogComponent, {
      width: '450px',
      disableClose: true,
      data: {cart: this.cart}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Create Product dialog was closed: ' + JSON.stringify(result));
      if (result) {
        const order = new Order({
          userId: this.currentUser.uid
          , userName: this.currentUser.displayName
          , billingAddress: result.billingAddress
          , shippingAddress: result.shippingAddress
          , productList: result.productList
          , comment: result.comment
          , fullPrice: result.fullPrice
          , status: 'New'
          , token: result.token.id
        });
        this.store.dispatch(new data.CreateOrderAction(order));
      }
    });
  }

  logout() {
    console.log('Logout');
    this.authenticationService.logout();
  }

  editUser() {
    console.log('Edit User');
    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '350px',
      disableClose: true,
      data: {
        user: this.currentUser
        , toolbar: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Edit dialog was closed: ' + JSON.stringify(result));
      if (result) {
        this.store.dispatch(new data.ChangeUserAction(result));
      }
    });
  }
}
