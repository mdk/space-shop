import {Component, Inject, OnInit, OnDestroy} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {clone} from 'lodash';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import {AppUser} from '../../model/AppUser';
import 'rxjs/add/operator/takeWhile';
import {CommonService} from '../../core/common.service';

@Component({
  selector: 'app-cart-dialog',
  templateUrl: './cart-dialog.component.html',
  styleUrls: ['./cart-dialog.component.scss']
})
export class CartDialogComponent implements OnInit, OnDestroy {
  cart: any;
  public currentUser: AppUser;
  public alive = true;

  constructor(public dialogRef: MdDialogRef<CartDialogComponent>
    , @Inject(MD_DIALOG_DATA) public data: any
    , private store: Store<fromRoot.State>
    , private common: CommonService
  ) {
    this.cart = clone(data.cart);
    if (!this.cart) {
      this.cart = {};
    }
    this.store.select(fromRoot.getCurrentUser)
      .takeWhile(() => this.alive)
      .subscribe(user => {
          this.currentUser = user;
          this.cart.billingAddress = this.currentUser.billingAddress;
          this.cart.shippingAddress = this.currentUser.shippingAddress;
        });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  onOkClick(): void {
    if (this.cart) {
      const dialog = this;
      const handler = (<any>window).StripeCheckout.configure({
        key: this.common.getEnv().STRIPE_KEY,
        locale: 'auto',
        token: function (token: any) {
          dialog.cart.token = token;
          dialog.dialogRef.close(dialog.cart);
        }
      });

      handler.open({
        name: 'Space Shop',
        description: this.cart.count + ' items',
        amount: this.cart.fullPrice * 100 * 1000000
      });
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
