import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {clone} from 'lodash';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent implements OnInit {
  user: any;
  edit = false;
  toolbar = false;

  constructor(public dialogRef: MdDialogRef<EditDialogComponent>,
              @Inject(MD_DIALOG_DATA) public data: any) {
    this.user = clone(data.user);
    if (!this.user) {
      this.user = {};
    }
    this.edit = data.edit;
    this.toolbar = data.toolbar;
    if (this.toolbar) {
      this.edit = true;
    }
  }

  ngOnInit() {
  }

  onOkClick(): void {
    if (this.user
      && this.user.displayName
      && this.user.email
      && ((!this.edit && this.user.password) || this.edit)
    ) {
      this.dialogRef.close(this.user);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
