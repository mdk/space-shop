import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import {EditDialogComponent} from './user-edit-dialog/edit-dialog.component';
import {
  MdButtonModule, MdDialogModule, MdFormFieldModule, MdIconModule, MdInputModule, MdPaginatorModule,
  MdTableModule, MdCheckboxModule, MdSelectModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import { ProductDialogComponent } from './product-dialog/product-dialog.component';
import { OrderDialogComponent } from './order-dialog/order-dialog.component';
import { CartDialogComponent } from './cart-dialog/cart-dialog.component';

@NgModule({
  imports: [
    CommonModule
    , FormsModule
    , MdTableModule
    , MdPaginatorModule
    , MdButtonModule
    , MdFormFieldModule
    , MdInputModule
    , MdIconModule
    , MdDialogModule
    , MdCheckboxModule
    , MdSelectModule
  ],
  declarations: [
    ConfirmDialogComponent
    , EditDialogComponent
    , ProductDialogComponent
    , OrderDialogComponent
    , CartDialogComponent
  ],
  entryComponents: [
    ConfirmDialogComponent
    , EditDialogComponent
    , ProductDialogComponent
    , OrderDialogComponent
    , CartDialogComponent
  ],
})
export class ComponentsModule { }
