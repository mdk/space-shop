import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {clone} from 'lodash';
import {AppUser} from '../../model/AppUser';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import 'rxjs/add/operator/takeWhile';

@Component({
  selector: 'app-order-dialog',
  templateUrl: './order-dialog.component.html',
  styleUrls: ['./order-dialog.component.scss']
})
export class OrderDialogComponent implements OnInit, OnDestroy {
  public alive = true;
  statusList = ['New', 'Delivered', 'Closed'];
  order: any;
  edit = false;
  public currentUser: AppUser;

  constructor(public dialogRef: MdDialogRef<OrderDialogComponent>
              , @Inject(MD_DIALOG_DATA) public data: any
              , private store: Store<fromRoot.State>
  ) {
    this.order = clone(data.order);
    if (!this.order) {
      this.order = {};
    }
    this.edit = data.edit;
    this.store.select(fromRoot.getCurrentUser)
      .takeWhile(() => this.alive)
      .subscribe(user => {
        this.currentUser = user;
      });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  onOkClick(): void {
    if (this.order) {
      this.dialogRef.close(this.order);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
