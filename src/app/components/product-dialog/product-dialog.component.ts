import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {clone} from 'lodash';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.scss']
})
export class ProductDialogComponent implements OnInit {
  product: any;
  edit = false;

  constructor(public dialogRef: MdDialogRef<ProductDialogComponent>,
              @Inject(MD_DIALOG_DATA) public data: any) {
    this.product = clone(data.product);
    if (!this.product) {
      this.product = {};
    }
    this.edit = data.edit;
  }

  ngOnInit() {
  }

  onOkClick(): void {
    if (this.product
      && this.product.title
    ) {
      this.dialogRef.close(this.product);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
