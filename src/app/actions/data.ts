import {type} from './util';
import {Action} from '@ngrx/store';
import {AppUser} from '../model/AppUser';

export const ActionTypes = {
  /* Users */
  LOAD_USERS: type('[Data] Load Users')
  , LOAD_USERS_SUCCESS: type('[Data] Load Users Success')
  , SET_CURRENT_USER: type('[Data] Set Current User')
  , CREATE_USER: type('[Data] Create User')
  , CREATE_USER_SUCCESS: type('[Data] Create User Success')
  , REMOVE_USER: type('[Data] Remove User')
  , REMOVE_USER_SUCCESS: type('[Data] Remove User Success')
  , CHANGE_USER: type('[Data] Change User')
  , CHANGE_USER_SUCCESS: type('[Data] Change User Success')
  , UPDATE_USERS: type('[Data] Update Users')
  , USERS_SERVER_FAIL: type('[Data] Users Server Failure')
  /* Products */
  , LOAD_PRODUCTS: type('[Data] Load Products')
  , LOAD_PRODUCTS_SUCCESS: type('[Data] Load Products Success')
  , CREATE_PRODUCT: type('[Data] Create Products')
  , REMOVE_PRODUCT: type('[Data] Remove Products')
  , CHANGE_PRODUCT: type('[Data] Change Products')
  , CHANGE_PRODUCT_SUCCESS: type('[Data] Change Products Success')
  , UPDATE_PRODUCTS: type('[Data] Update Products')
  , PRODUCTS_SERVER_FAIL: type('[Data] Products Server Failure')
  /* Orders */
  , LOAD_ORDERS: type('[Data] Load Orders')
  , LOAD_ORDERS_SUCCESS: type('[Data] Load Orders Success')
  , CREATE_ORDER: type('[Data] Create Orders')
  , CREATE_ORDER_SUCCESS: type('[Data] Create Order Success')
  , REMOVE_ORDER: type('[Data] Remove Orders')
  , CHANGE_ORDER: type('[Data] Change Orders')
  , CHANGE_ORDER_SUCCESS: type('[Data] Change Orders Success')
  , UPDATE_ORDERS: type('[Data] Update Orders')
  , ORDERS_SERVER_FAIL: type('[Data] Orders Server Failure')

};

/**
 * Users
 */

export class LoadUsersAction implements Action {
  type = ActionTypes.LOAD_USERS;
}

export class LoadUsersSuccessAction implements Action {
  type = ActionTypes.LOAD_USERS_SUCCESS;

  constructor(public payload: any) {
  }
}

export class SetCurrentUserAction implements Action {
  type = ActionTypes.SET_CURRENT_USER;

  constructor(public payload: AppUser) {
  }
}

export class UpdateAction implements Action {
  type = ActionTypes.UPDATE_USERS;

  constructor(public payload: any) {
  }
}

export class CreateUserAction implements Action {
  type = ActionTypes.CREATE_USER;

  constructor(public payload: any) {
  }
}

export class CreateUserSuccessAction implements Action {
  type = ActionTypes.CREATE_USER_SUCCESS;

  constructor(public payload: any) {
  }
}

export class RemoveUserAction implements Action {
  type = ActionTypes.REMOVE_USER;

  constructor(public payload: any) {
  }
}

export class RemoveUserSuccessAction implements Action {
  type = ActionTypes.REMOVE_USER_SUCCESS;

  constructor(public payload: any) {
  }
}

export class ChangeUserAction implements Action {
  type = ActionTypes.CHANGE_USER;

  constructor(public payload: any) {
  }
}

export class ChangeUserSuccessAction implements Action {
  type = ActionTypes.CHANGE_USER_SUCCESS;

  constructor(public payload: any) {
  }
}

export class UsersServerFailAction implements Action {
  type = ActionTypes.USERS_SERVER_FAIL;

  constructor(public payload: any) {
  }
}

/**
 * Products
 */

export class LoadProductsAction implements Action {
  type = ActionTypes.LOAD_PRODUCTS;
}

export class LoadProductsSuccessAction implements Action {
  type = ActionTypes.LOAD_PRODUCTS_SUCCESS;

  constructor(public payload: any) {
  }
}

export class UpdateProductsAction implements Action {
  type = ActionTypes.UPDATE_PRODUCTS;

  constructor(public payload: any) {
  }
}

export class CreateProductAction implements Action {
  type = ActionTypes.CREATE_PRODUCT;

  constructor(public payload: any) {
  }
}

export class RemoveProductAction implements Action {
  type = ActionTypes.REMOVE_PRODUCT;

  constructor(public payload: any) {
  }
}

export class ChangeProductAction implements Action {
  type = ActionTypes.CHANGE_PRODUCT;

  constructor(public payload: any) {
  }
}

export class ChangeProductSuccessAction implements Action {
  type = ActionTypes.CHANGE_PRODUCT_SUCCESS;

  constructor(public payload: any) {
  }
}

export class ProductServerFailAction implements Action {
  type = ActionTypes.PRODUCTS_SERVER_FAIL;

  constructor(public payload: any) {
  }
}
/**
 * Orders
 */

export class LoadOrdersAction implements Action {
  type = ActionTypes.LOAD_ORDERS;
}

export class LoadOrdersSuccessAction implements Action {
  type = ActionTypes.LOAD_ORDERS_SUCCESS;

  constructor(public payload: any) {
  }
}

export class UpdateOrdersAction implements Action {
  type = ActionTypes.UPDATE_ORDERS;

  constructor(public payload: any) {
  }
}

export class CreateOrderAction implements Action {
  type = ActionTypes.CREATE_ORDER;

  constructor(public payload: any) {
  }
}

export class CreateOrderSuccessAction implements Action {
  type = ActionTypes.CREATE_ORDER_SUCCESS;

  constructor(public payload: any) {
  }
}

export class RemoveOrderAction implements Action {
  type = ActionTypes.REMOVE_ORDER;

  constructor(public payload: any) {
  }
}

export class ChangeOrderAction implements Action {
  type = ActionTypes.CHANGE_ORDER;

  constructor(public payload: any) {
  }
}

export class ChangeOrderSuccessAction implements Action {
  type = ActionTypes.CHANGE_ORDER_SUCCESS;

  constructor(public payload: any) {
  }
}

export class OrdersServerFailAction implements Action {
  type = ActionTypes.ORDERS_SERVER_FAIL;

  constructor(public payload: any) {
  }
}


export type Actions
  /* Users */
  = LoadUsersAction
  | LoadUsersSuccessAction
  | SetCurrentUserAction
  | CreateUserAction
  | CreateUserSuccessAction
  | RemoveUserAction
  | RemoveUserSuccessAction
  | ChangeUserAction
  | ChangeUserSuccessAction
  | UpdateAction
  | UsersServerFailAction
  /* Products */
  | LoadProductsAction
  | LoadProductsSuccessAction
  | CreateProductAction
  | RemoveProductAction
  | ChangeProductAction
  | ChangeProductSuccessAction
  | UpdateProductsAction
  | ProductServerFailAction
  /* Orders */
  | LoadOrdersAction
  | LoadOrdersSuccessAction
  | CreateOrderAction
  | CreateOrderSuccessAction
  | RemoveOrderAction
  | ChangeOrderAction
  | ChangeOrderSuccessAction
  | UpdateOrdersAction
  | OrdersServerFailAction

  ;
