import {type} from './util';
import {Action} from '@ngrx/store';

export const ActionTypes = {
  SET_TOOLBAR_COLOR: type('[UI] Set Toolbar Color')
  , SET_PHOTO_URL: type('[UI] Set Photo URL')
  , SET_CART: type('[UI] Set Cart')
  , ADD_PRODUCT_TO_CART: type('[UI] Add Product To Cart')
};

export class SetToolbarColorAction implements Action {
  type = ActionTypes.SET_TOOLBAR_COLOR;

  constructor(public payload: string) {
  }
}

export class SetPhotoURLAction implements Action {
  type = ActionTypes.SET_PHOTO_URL;

  constructor(public payload: string) {
  }
}

export class SetCartAction implements Action {
  type = ActionTypes.SET_CART;

  constructor(public payload: string) {
  }
}

export class AddProductToCartAction implements Action {
  type = ActionTypes.ADD_PRODUCT_TO_CART;

  constructor(public payload: any) {
  }
}

export type Actions
  = SetToolbarColorAction
  | SetPhotoURLAction
  | SetCartAction
  | AddProductToCartAction
  ;
