import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as fromRoot from '../reducers';
import {Store} from '@ngrx/store';
import 'rxjs/add/operator/takeWhile';
import {AppUser} from '../model/AppUser';
import {UsersDataService} from '../services/users-data.service';
import {MdDialog, MdPaginator} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {EditDialogComponent} from '../components/user-edit-dialog/edit-dialog.component';
import {CommonService} from '../core/common.service';
import * as data from '../actions/data';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  public alive = true;
  displayedColumns = ['email', 'displayName', 'actions'];
  dataSource: UsersDataSource | null;

  @ViewChild(MdPaginator) paginator: MdPaginator;

  constructor(private store: Store<fromRoot.State>
    , private dataService: UsersDataService
    , public dialog: MdDialog
    , public common: CommonService) {
  }

  ngOnInit() {
    this.dataService.dispatchLoad();
    this.dataSource = new UsersDataSource(this.store, this.paginator);
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  editUser(row: any) {
    console.log('Edit: ' + JSON.stringify(row));
    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '350px',
      disableClose: true,
      data: {
        user: row
        , edit: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Edit dialog was closed: ' + JSON.stringify(result));
      if (result) {
        this.store.dispatch(new data.ChangeUserAction(result));
      }
    });
  }

  removeUser(row: any) {
    this.common.confirm('Remove User', `Are you sure you want to delete user '${row.displayName}'?`)
      .take(1)
      .subscribe(res => {
        console.log('Remove: ' + JSON.stringify(res));
        if (res) {
          this.store.dispatch(new data.RemoveUserAction(row.uid));
        }
      });
  }

  createUser() {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '350px',
      disableClose: true,
      data: {user: {}}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Create dialog was closed: ' + JSON.stringify(result));
      if (result) {
        this.store.dispatch(new data.CreateUserAction(result));
      }
    });
  }
}

export class UsersDataSource extends DataSource<any> implements OnDestroy {
  public alive = true;
  public users: Array<AppUser> = [];
  dataChange: BehaviorSubject<AppUser[]> = new BehaviorSubject<AppUser[]>([]);

  constructor(private store: Store<fromRoot.State>, private _paginator: MdPaginator) {
    super();
    this.store.select(fromRoot.getUsers)
      .takeWhile(() => this.alive)
      .subscribe(val => {
        this.users = val;
        this.dataChange.next(this.users);
      });
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<AppUser[]> {
    const displayDataChanges = [
      this.dataChange,
      this._paginator.page,
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      const data = this.users.slice();

      // Grab the page's slice of data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      return data.splice(startIndex, this._paginator.pageSize);
    });
  }

  disconnect() {
  }
}
