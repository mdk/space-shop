import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersComponent} from './users.component';
import {RouterModule, Routes} from '@angular/router';
import {CanActivateAuthGuard} from '../login/can-activate.authguard';
import {
  MdButtonModule, MdDialogModule, MdFormFieldModule, MdIconModule, MdInputModule, MdPaginatorModule,
  MdTableModule, MdCheckboxModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {ComponentsModule} from '../components/components.module';

const routes: Routes = [
  {path: '', component: UsersComponent, canActivate: [CanActivateAuthGuard]}
];

@NgModule({
  imports: [
    CommonModule
    , FormsModule
    , RouterModule.forChild(routes)
    , ComponentsModule
    , MdTableModule
    , MdPaginatorModule
    , MdButtonModule
    , MdFormFieldModule
    , MdInputModule
    , MdIconModule
    , MdDialogModule
    , MdCheckboxModule
  ],
  declarations: [
    UsersComponent
  ],
  entryComponents: [
    UsersComponent
  ]
})
export class UsersModule {
}
