import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LoginComponent} from './login/login.component';
import {CanActivateAuthGuard} from './login/can-activate.authguard';
import {RegisterComponent} from './login/register.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'}
  , {path: 'login', component: LoginComponent}
  , {path: 'register', component: RegisterComponent}
  , {path: 'users', loadChildren: './users/users.module#UsersModule', canActivate: [CanActivateAuthGuard]}
  , {path: 'orders', loadChildren: './orders/orders.module#OrdersModule', canActivate: [CanActivateAuthGuard]}
  , {path: 'dashboard', loadChildren: './product-list/product-list.module#ProductListModule', canActivate: [CanActivateAuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
