import {Component, ViewChild} from '@angular/core';
import {AuthenticationService} from './login/authentication.service';
import {MdSidenav} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public auth: any;
  @ViewChild(MdSidenav) sidenav: MdSidenav;

  constructor(private router: Router, private authenticationService: AuthenticationService) {
    this.authenticationService.getAuthenticated().subscribe((auth) => {
      this.auth = auth;
      if (!auth) {
        this.sidenav.close();
        this.router.navigate(['/login']);
      }
    });
  }
}
