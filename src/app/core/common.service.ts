import {AuthenticationService} from '../login/authentication.service';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {UtilsService} from './utils.service';
import {ToasterService} from 'angular2-toaster';
import {Observable} from 'rxjs/Observable';
import {MdDialog, MdDialogRef} from '@angular/material';
import {ConfirmDialogComponent} from '../components/confirm-dialog/confirm-dialog.component';

@Injectable()
export class CommonService {

  constructor(public auth: AuthenticationService
    , public http: Http
    , public utils: UtilsService
    , public toaster: ToasterService
    , private dialog: MdDialog
  ) {
  }

  public getEnv() {
    return this.utils.getEnv();
  }

  public showMessage(type: string, title: string, body?: string) {
    console.log('Message ' + type + ': ' + title);
    this.toaster.pop(type, title, body);
  }

  showError(error: any) {
    if (error instanceof Error) {
      this.showMessage('error', error.name, error.message);
    } else {
      this.showMessage('error', error);
    }
  }

  public confirm(title: string, message: string): Observable<boolean> {

    let dialogRef: MdDialogRef<ConfirmDialogComponent>;

    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.message = message;

    return dialogRef.afterClosed();
  }

}
