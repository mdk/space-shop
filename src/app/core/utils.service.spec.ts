import { TestBed, inject } from '@angular/core/testing';

import { UtilsService } from './utils.service';

describe('UtilsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UtilsService]
    });
  });

  it('should be created', inject([UtilsService], (service: UtilsService) => {
    expect(service).toBeTruthy();
  }));

  it('test environment', inject([UtilsService], (service: UtilsService) => {
    const env = service.getEnv();
    expect(env).toBeTruthy();
    expect(env.production).toBeFalsy();
  }));
});
