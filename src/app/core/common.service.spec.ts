import {TestBed, inject} from '@angular/core/testing';

import {CommonService} from './common.service';
import {MockBackend} from '@angular/http/testing';
import {AuthenticationService} from '../login/authentication.service';
import {UtilsService} from './utils.service';
import {Http} from '@angular/http';

describe('CommonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CommonService
        , {provide: Http, useClass: MockBackend}
        , AuthenticationService
        , UtilsService
        ]
    });
  });

  it('should be created', inject([CommonService], (service: CommonService) => {
    expect(service).toBeTruthy();
  }));

  it('has environment', inject([CommonService], (service: CommonService) => {
    const env = service.getEnv();
    expect(env).toBeTruthy();
    expect(env.test).toBeTruthy();
  }));

  it('has links', inject([CommonService], (service: CommonService) => {
    expect(service.http).toBeTruthy();
    expect(service.auth).toBeTruthy();
    expect(service.utils).toBeTruthy();
  }));
});
