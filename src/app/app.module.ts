import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import {reducer} from './reducers/index';
import {AppRoutingModule} from './app-routing.module';
import {HttpModule} from '@angular/http';
import {LoginComponent} from './login/login.component';
import {CommonService} from './core/common.service';
import {AuthenticationService} from './login/authentication.service';
import {CanActivateAuthGuard} from './login/can-activate.authguard';
import {UtilsService} from './core/utils.service';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {environment} from '../environments/environment';
import {RegisterComponent} from './login/register.component';
import {RecaptchaModule} from 'ng-recaptcha';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MdButtonModule, MdCardModule, MdDialogModule, MdFormFieldModule, MdIconModule, MdInputModule, MdListModule,
  MdSidenavModule,
  MdToolbarModule
} from '@angular/material';
import {ToasterModule} from 'angular2-toaster';
import {MenuComponent} from './menu/menu.component';
import {ToolbarMenuComponent} from './toolbar-menu/toolbar-menu.component';
import {UsersDataService} from './services/users-data.service';
import {ComponentsModule} from './components/components.module';
import {OrdersDataService} from './services/orders-data.service';
import {ProductsDataService} from './services/products-data.service';
import {EffectsModule} from '@ngrx/effects';
import {OrdersDataEffects} from './services/orders-data-effects.service';
import {ProductsDataEffects} from './services/products-data-effects.service';
import {UsersDataEffects} from './services/users-data-effects.service';

@NgModule({
  declarations: [
    AppComponent
    , LoginComponent
    , RegisterComponent
    , MenuComponent
    , ToolbarMenuComponent
  ],
  imports: [
    BrowserModule
    , AngularFireModule.initializeApp(environment.firebase)
    , AngularFireDatabaseModule
    , AngularFireAuthModule
    , EffectsModule.run(OrdersDataEffects)
    , EffectsModule.run(ProductsDataEffects)
    , EffectsModule.run(UsersDataEffects)
    , FormsModule
    , AppRoutingModule
    , HttpModule
    , ToasterModule
    , StoreModule.provideStore(reducer)
    , RecaptchaModule.forRoot()
    , BrowserAnimationsModule
    , ComponentsModule
    , MdButtonModule
    , MdFormFieldModule
    , MdInputModule
    , MdCardModule
    , MdToolbarModule
    , MdIconModule
    , MdSidenavModule
    , MdListModule
    , MdDialogModule
  ],
  providers: [
    CommonService
    , AuthenticationService
    , CanActivateAuthGuard
    , UtilsService
    , UsersDataService
    , ProductsDataService
    , OrdersDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
