import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './product-list.component';
import {RouterModule, Routes} from '@angular/router';
import {CanActivateAuthGuard} from '../login/can-activate.authguard';
import { ProductComponent } from './product/product/product.component';
import {FormsModule} from '@angular/forms';
import {MdButtonModule, MdCardModule, MdIconModule} from '@angular/material';

const routes: Routes = [
  {path: '', component: ProductListComponent, canActivate: [CanActivateAuthGuard]}
];

@NgModule({
  imports: [
    CommonModule
    , FormsModule
    , RouterModule.forChild(routes)
    , MdCardModule
    , MdIconModule
    , MdButtonModule
  ],
  declarations: [
    ProductListComponent
    , ProductComponent
  ]
})
export class ProductListModule { }
