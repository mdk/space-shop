import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductsDataService} from '../services/products-data.service';
import {CommonService} from '../core/common.service';
import {MdDialog} from '@angular/material';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import {Product} from '../model/Product';
import 'rxjs/add/operator/takeWhile';
import {ProductDialogComponent} from '../components/product-dialog/product-dialog.component';
import * as data from '../actions/data';
import {AppUser} from '../model/AppUser';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {
  public alive = true;
  public products: Array<Product> = [];
  public currentUser: AppUser;

  constructor(private dataService: ProductsDataService
    , private store: Store<fromRoot.State>
    , public dialog: MdDialog
    , public common: CommonService) {

    this.store.select(fromRoot.getProducts)
      .takeWhile(() => this.alive)
      .subscribe(val => {
        this.products = val;
      });
    this.store.select(fromRoot.getCurrentUser)
      .takeWhile(() => this.alive)
      .subscribe(user => {
        this.currentUser = user;
      });
  }

  ngOnInit() {
    this.dataService.dispatchLoad();
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  createProduct() {
    console.log('Create Product');
    const dialogRef = this.dialog.open(ProductDialogComponent, {
      width: '350px',
      disableClose: true,
      data: {product: {}}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Create Product dialog was closed: ' + JSON.stringify(result));
      if (result) {
        this.store.dispatch(new data.CreateProductAction(result));
      }
    });
  }
}
