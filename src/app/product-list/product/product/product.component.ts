import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Product} from '../../../model/Product';
import {CommonService} from '../../../core/common.service';
import * as data from '../../../actions/data';
import * as ui from '../../../actions/ui';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {ProductDialogComponent} from '../../../components/product-dialog/product-dialog.component';
import {MdDialog} from '@angular/material';
import {AppUser} from '../../../model/AppUser';
import 'rxjs/add/operator/takeWhile';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
  public alive = true;
  @Input() product: Product;
  public currentUser: AppUser;

  constructor(private store: Store<fromRoot.State>
    , public dialog: MdDialog
    , public common: CommonService) {
    this.store.select(fromRoot.getCurrentUser)
      .takeWhile(() => this.alive)
      .subscribe(user => {
        this.currentUser = user;
      });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  editProduct() {
    console.log('Edit: ' + JSON.stringify(this.product));
    const dialogRef = this.dialog.open(ProductDialogComponent, {
      width: '350px',
      disableClose: true,
      data: {
        product: this.product
        , edit: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The Edit Product dialog was closed: ' + JSON.stringify(result));
      if (result) {
        this.store.dispatch(new data.ChangeProductAction(result));
      }
    });
  }

  removeProduct() {
    console.log('Remove Product');
    this.common.confirm('Remove Product', `Are you sure you want to delete product '${this.product.title}'?`)
      .take(1)
      .subscribe(res => {
        console.log('Remove: ' + JSON.stringify(res));
        if (res && this.product.uid) {
          this.store.dispatch(new data.RemoveProductAction(this.product.uid));
        }
      });
  }

  addToCart() {
    console.log('Add To Cart');
    this.store.dispatch(new ui.AddProductToCartAction(this.product));
  }
}
